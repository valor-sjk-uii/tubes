<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'jarkom_wp_db' );

/** MySQL database username */
define( 'DB_USER', 'jarkom_wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'LPLMdz4)q_R&B$K+Bf7sm7Cw*5z5]|$hqdBro@=Hk/mnOu0<z~OTNR=MWA#l %fj' );
define( 'SECURE_AUTH_KEY',  '}R`F/2FiQFOk%E--Cwn9<d|r_ {ok~b@}?q&D[H`,8G<!;EW5/ wO[~h)/gi<`dg' );
define( 'LOGGED_IN_KEY',    'Z}8~-WpmV?h~gGS*%udo;/wXgTi7WF8hrxzJ:dL<{_;.[V5br/7r8ma:!{Bc#$=~' );
define( 'NONCE_KEY',        'PvQ8fj!^;AV.m/.|AVp`8dE{h3DGM-QC|yc/)#90O(H4[b#G&@i,f/=.VM%)u+bS' );
define( 'AUTH_SALT',        'd%n;|5=S./iA9S|,/LnD%a^0_Gk;mI|sO(/r=t@r}70IVUXylR-P6II)xD~1Bnp%' );
define( 'SECURE_AUTH_SALT', 'L4dE=no`ywkTxfi lx_1jg~FD2u okN..[d%n^fVCY:S-l]sXpd)FmZ~]W=HiM/k' );
define( 'LOGGED_IN_SALT',   'g3UvA4!3<B]SQG6wI>{XqFca9-l UJIU edQ!Gl*|vU`-,m.-i2OL$=,-LS2~;tw' );
define( 'NONCE_SALT',       ']!9S{ckfM`R]*cGys7sQu+ulV|@[g#9M45Mk`,eFowHBudPjz@T}5lkO-:@AY:Bl' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
