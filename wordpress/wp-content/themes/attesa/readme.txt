=== Attesa ===
Contributors: crestaproject
Tags: two-columns, right-sidebar, custom-colors, custom-menu, featured-images, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready, blog, news, e-commerce, footer-widgets, custom-logo
Requires at least: 4.5
Tested up to: 5.6
Stable tag: 1.3.2
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Attesa WordPress Theme, Copyright 2020 Rizzo Andrea
Attesa is distributed under the terms of the GNU GPL

== Description ==

Attesa is an extremely versatile and customizable multipurpose WordPress theme. You can use it to create a simple blog but also for more complex websites or even an e-commerce using WooCommerce. You can customize many graphic aspects to create the website that best suits your target. Attesa theme is compatible with the most famous page builders as Elementor, SiteOrigin, Beaver Builder, etc... Responsive, RTL ready, WPML ready, SEO oriented and with many available demos.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Attesa includes support for Infinite Scroll in Jetpack, WooCommerce, Easy Digital Downlods and WPML.

== Resources ==
* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
* FontAwesome http://fontawesome.io/ Font licensed under SIL OFL 1.1 and Code lisensed under MIT
* nanoScrollerJS https://github.com/jamesflorentino/nanoScrollerJS licensed under MIT
* SmoothScroll https://github.com/gblazex/smoothscroll-for-websites licensed under MIT
* Customize section button https://github.com/WPTRT/customize-section-button licensed under the GNU GPL, version 2 or later

== Screenshots == 
Image used from pxhere.com
License: Pxhere.com CC0 license https://creativecommons.org/publicdomain/zero/1.0/
Image Source: https://pxhere.com/en/photo/1565417

== Changelog ==

= Version 1.3.2 =
* Fixed a bug with featured title menu option
* Minor bug fixes

= Version 1.3.1 =
* Improved compatibility with WordPress 5.6 and new jQuery

= Version 1.3.0 =
* Updated WooCommerce template

= Version 1.2.9 =
* Fixed a visual bug with anchors in the menu
* Updated FontAwesome to 5.14.0 version

= Version 1.2.8 =
* Improved logo on scroll

= Version 1.2.7 =
* Updated FontAwesome to 5.13.1 version
* Added TikTok social button

= Version 1.2.6 =
* Added WhatsApp social button
* Added compatibility with "Custom Fonts" plugin to upload custom fonts
* Minor bug fixes

= Version 1.2.5 =
* Improved theme hooks management
* Minor bug fixes

= Version 1.2.4 =
* Improved WooCommerce style on category products
* Now copyright text accepts shortcodes

= Version 1.2.3 =
* Fixed a bug with WooCommerce pages on mobile view

= Version 1.2.2 =
* Updated FontAwesome to 5.13.0 version
* Created a separate style file for WooCommerce and EDD
* Minor bug fixes

= Version 1.2.1 =
* Added Spotify social button
* Minor bug fixes

= Version 1.2.0 =
* Minor bug fixes

= Version 1.1.9 =
* Added the possibility to show/hide the meta for posts
* Minor bug fixes

= Version 1.1.8 =
* Minor bug fixes

= Version 1.1.7 =
* Improved theme customization with hooks
* Minor bug fixes

= Version 1.1.6 =
* Added the possibility to add sub-menu effect on hover
* Improved WooCommerce style in My Account page
* Minor bug fixes

= Version 1.1.5 =
* Added filter to search FontAwesome icons
* Minor bug fixes

= Version 1.1.4 =
* Improved CSS style
* Minor bug fixes

= Version 1.1.3 =
* Added possibility to change the header colors individually

= Version 1.1.2 =
* Minor bug fixes

= Version 1.1.1 =
* Created a hook for entry_header

= Version 1.1.0 =
* Fixed a bug with the double logo

= Version 1.0.9 =
* Improved main menu and sub-menus position
* Minor bug fixes

= Version 1.0.8 =
* Improved compatibility with WordPress 5.3
* Added new menu link style (minimal side)
* Added new Google Fonts
* Minor bug fixes

= Version 1.0.7 =
* Improved main menu and sub-menus position
* Added an option to add an alternative logo when the user scrolls
* Updated SmoothScroll to 1.4.10 version

= Version 1.0.6 =
* Added an option to invert the top bar position
* Added an option to choose custom text for mobile menu
* Added an option to choose custom icon for mobile menu
* Added an option to choose the Push Sidebar icon button
* Update WooCommerce template (tabs.php)

= Version 1.0.5 =
* Updated FontAwesome to 5.11.1 version
* Minor bug fixes

= Version 1.0.4 =
* Updated WooCommerce mini-cart.php template
* Added an option to choose the widgets title heading

= Version 1.0.3 =
* Updated FontAwesome to 5.10.1 version
* Minor bug fixes

= Version 1.0.2 =
* Added the possibility to show/hide the sub-footer (thanks to Marco Montanari)
* Added code compatibility for the new version of Attesa PRO
* Updated FontAwesome to 5.10.0 version

= Version 1.0.1 =
* Fixed a bug with Elementor PRO
* Minor bug fixes

= Version 1.0.0 =
* Initial release
